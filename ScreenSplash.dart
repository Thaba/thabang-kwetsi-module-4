Column(
  mainAxisSize: MainAxisSize.max,
  children: [
    Expanded(
      child: Align(
        alignment: AlignmentDirectional(0, 1),
        child: Image.network(
          'https://picsum.photos/seed/765/600',
          width: 100,
          height: 100,
          fit: BoxFit.cover,
        ),
      ),
    ),
    Expanded(
      child: Align(
        alignment: AlignmentDirectional(0, -0.15),
        child: Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            color: Color(0xFFEEEEEE),
          ),
        ),
      ),
    ),
  ],
)

